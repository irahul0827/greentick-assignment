import 'package:flutter/material.dart';
import 'package:flutter_assignment/models/retailShopData.dart';
import 'package:flutter_assignment/productCard.dart';
import 'package:flutter_assignment/sampleData.dart';
import 'package:flutter_assignment/utils/colors.dart';
import 'package:flutter_assignment/utils/fontStyles.dart';

class ShopListView extends StatelessWidget {
  const ShopListView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(
          color: kDividerColor,
          thickness: 1,
        ),
        Row(
          children: [
            SizedBox(width: 10),
            Expanded(
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  Icon(
                    Icons.thumb_up,
                    color: kRedColor,
                  ),
                  SizedBox(width: 15),
                  Text(
                    'Recommended for You',
                    style: kHeadingText,
                  ),
                ],
              ),
            ),
            TextButton(
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text("Shops List"),
                  ),
                );
              },
              child: Text(
                "View All",
                style: TextStyle(
                  color: kRedColor,
                ),
              ),
            )
          ],
        ),
        StreamBuilder<List<ShopModel>>(
            stream: getData(),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data != null) {
                return Container(
                  height: 250,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (_, index) => Container(
                      padding: EdgeInsets.all(10),
                      child: ProductCard(
                        productData: snapshot.data![index],
                      ),
                    ),
                  ),
                );
              }
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(width: 10),
                    Text("Loading..."),
                  ],
                ),
              );
            }),
        Divider(
          color: kDividerColor,
          thickness: 3,
        ),
      ],
    );
  }

  Stream<List<ShopModel>> getData() async* {
    yield await Future.delayed(
      Duration(seconds: 1),
      () => sampleData.map((e) => ShopModel.fromData(e)).toList(),
    );
  }
}
