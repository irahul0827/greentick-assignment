const List<Map> sampleData = [
  {
    "title": "Markiza Spa, Sports City",
    "likeCount": 978,
    "dislikeCount": 103,
    "rating": 4.3,
    "image": "https://xstressmassage.com/images/services-02.jpg",
  },
  {
    "title": "W Dubai - The Palm",
    "likeCount": 99,
    "dislikeCount": 0,
    "rating": 4.5,
    "image":
        "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1c/de/ea/68/wet.jpg?w=900&h=-1&s=1"
  },
  {
    "title": "Butterfly Garden Dubai",
    "likeCount": 700,
    "dislikeCount": 1,
    //no ratings here
    "image":
        "https://blogbox.ieagle.com/wp-content/uploads/2016/02/Butterfly-Garden-among-dubai-tourist-places-to-visit.jpg"
  },
  {
    "title": "Rixos Premium, Dubai",
    "likeCount": 534,
    "dislikeCount": 12,
    "rating": 4.9,
    "image":
        "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1d/21/08/ba/exterior-view.jpg?w=900&h=-1&s=1"
  },
];
