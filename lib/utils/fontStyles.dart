import 'package:flutter/material.dart';
import 'package:flutter_assignment/utils/colors.dart';

const TextStyle kHeadingText = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.w600,
  color: kRedColor,
);

const TextStyle kTitleText = TextStyle(
  fontSize: 14,
  fontWeight: FontWeight.w600,
  color: kGreenColor,
);

const TextStyle kInfoText = TextStyle(
  fontSize: 14,
  // fontWeight: FontWeight.w300,
  color: Colors.grey,
);
