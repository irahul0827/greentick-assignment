import 'package:flutter/material.dart';
import 'package:flutter_assignment/shopsList.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 20),
            ShopListView(),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
